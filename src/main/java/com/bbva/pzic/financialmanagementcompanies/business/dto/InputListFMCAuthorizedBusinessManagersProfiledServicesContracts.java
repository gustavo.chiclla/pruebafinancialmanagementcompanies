package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class InputListFMCAuthorizedBusinessManagersProfiledServicesContracts {

    @Size(max = 8, groups = ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class)
    private String financialManagementCompanyId;

    @Size(max = 8, groups = ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class)
    private String authorizedBusinessManagerId;

    @Size(max = 4, groups = ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class)
    private String profiledServiceId;

    @Size(max = 35, groups = ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class)
    private String paginationKey;

    @Digits(integer = 3, fraction = 0, groups = ValidationGroup.ListFMCAuthorizedBusinessManagersProfiledServicesContracts.class)
    private Integer pageSize;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getAuthorizedBusinessManagerId() {
        return authorizedBusinessManagerId;
    }

    public void setAuthorizedBusinessManagerId(String authorizedBusinessManagerId) {
        this.authorizedBusinessManagerId = authorizedBusinessManagerId;
    }

    public String getProfiledServiceId() {
        return profiledServiceId;
    }

    public void setProfiledServiceId(String profiledServiceId) {
        this.profiledServiceId = profiledServiceId;
    }

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
