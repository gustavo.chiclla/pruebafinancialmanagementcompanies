package com.bbva.pzic.financialmanagementcompanies.business.dto;


import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.BusinessManagerSubscriptionRequest;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
public class InputModifyBusinessManagerSubscriptionRequest {

    private String subscriptionRequestId;
    private String businessManagerId;
    private BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest;

    public String getSubscriptionRequestId() {
        return subscriptionRequestId;
    }

    public void setSubscriptionRequestId(String subscriptionRequestId) {
        this.subscriptionRequestId = subscriptionRequestId;
    }

    public String getBusinessManagerId() {
        return businessManagerId;
    }

    public void setBusinessManagerId(String businessManagerId) {
        this.businessManagerId = businessManagerId;
    }

    public BusinessManagerSubscriptionRequest getBusinessManagerSubscriptionRequest() {
        return businessManagerSubscriptionRequest;
    }

    public void setBusinessManagerSubscriptionRequest(BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest) {
        this.businessManagerSubscriptionRequest = businessManagerSubscriptionRequest;
    }
}
