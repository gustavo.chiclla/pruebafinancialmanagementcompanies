package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;

public class InputRequestBackendRest {

    @Valid
    private DTOIntRequestHeader header;

    @Valid
    private DTOIntRequestBody requestBody;

    private String aap;

    public DTOIntRequestHeader getHeader() {
        return header;
    }

    public void setHeader(DTOIntRequestHeader header) {
        this.header = header;
    }

    public DTOIntRequestBody getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(DTOIntRequestBody requestBody) {
        this.requestBody = requestBody;
    }

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

}
