package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>KWHE</code>
 *
 * @see PeticionTransaccionKwhe
 * @see RespuestaTransaccionKwhe
 */
@Component("transaccionKwhe")
public class TransaccionKwhe implements InvocadorTransaccion<PeticionTransaccionKwhe, RespuestaTransaccionKwhe> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwhe invocar(PeticionTransaccionKwhe transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhe.class, RespuestaTransaccionKwhe.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwhe invocarCache(PeticionTransaccionKwhe transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhe.class, RespuestaTransaccionKwhe.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}