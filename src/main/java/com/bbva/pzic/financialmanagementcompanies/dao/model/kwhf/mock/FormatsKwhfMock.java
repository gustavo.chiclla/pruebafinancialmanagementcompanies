package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBSHF;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

public class FormatsKwhfMock {

    private static final FormatsKwhfMock INSTANCE = new FormatsKwhfMock();
    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private FormatsKwhfMock() {
    }

    public static FormatsKwhfMock getInstance() {
        return INSTANCE;
    }

    public List<FormatoKNECBSHF> getFormatoKNECBSHF() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/bbva/pzic/financialmanagementcompanies/dao/model/kwhf/mock/formatoKNECBSHF.json"), new TypeReference<List<FormatoKNECBSHF>>(){

        });
    }
}
