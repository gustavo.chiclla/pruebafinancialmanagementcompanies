package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.mock;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.Error;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBEHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.PeticionTransaccionKwhf;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.RespuestaTransaccionKwhf;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.RestDeleteGroupMock;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.RestDeleteUserMock;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component("transaccionKwhf")
public class TransaccionKwhfMock implements InvocadorTransaccion<PeticionTransaccionKwhf, RespuestaTransaccionKwhf> {

    public static final String THROW_ERROR = "6666";
    public static final String TEST_NO_RESULT = "7777";

    @Override
    public RespuestaTransaccionKwhf invocar(PeticionTransaccionKwhf transaccion) {
        try {
            RespuestaTransaccionKwhf response = new RespuestaTransaccionKwhf();
            response.setCodigoRetorno("OK_COMMIT");
            response.setCodigoControl("OK");

            FormatoKNECBEHF format = transaccion.getCuerpo().getParte(FormatoKNECBEHF.class);

            if (TEST_NO_RESULT.equalsIgnoreCase(format.getUsuid()))
                return response;

            if (THROW_ERROR.equalsIgnoreCase(format.getUsuid()) || isDelUserResponse(format.getUsuid())) {
                response.setCodigoRetorno("NO_OK_COMMIT");
                response.setCodigoControl("ERROR_FORMATO_SALIDA_NO_EXISTE");
                Error error = new Error();
                error.setCodigoError(THROW_ERROR);
                error.setDescripcionError("Error Mock");
                response.getCuerpo().getPartes().add(error);
            } else {
                response.getCuerpo().getPartes().addAll(buildCopies(FormatsKwhfMock.getInstance().getFormatoKNECBSHF()));
            }

            return response;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private boolean isDelUserResponse(String businessManagerId) {
        return RestDeleteUserMock.EMPTY_RESPONSE.equalsIgnoreCase(businessManagerId) ||
                RestDeleteGroupMock.EMPTY_RESPONSE.equalsIgnoreCase(businessManagerId);
    }

    private CopySalida buildCopy(final Object format) {
        CopySalida copy = new CopySalida();
        copy.setCopy(format);
        return copy;
    }

    private List<CopySalida> buildCopies(final List<?> formats) {
        List<CopySalida> copies = new ArrayList<>();
        for (Object format : formats) {
            copies.add(buildCopy(format));
        }
        return copies;
    }

    @Override
    public RespuestaTransaccionKwhf invocarCache(PeticionTransaccionKwhf peticionTransaccionKwhf) throws ExcepcionTransaccion {
        return null;
    }

    @Override
    public void vaciarCache() {

    }
}
