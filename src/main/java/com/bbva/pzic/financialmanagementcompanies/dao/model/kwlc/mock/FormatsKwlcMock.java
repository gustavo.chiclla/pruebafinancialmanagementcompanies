package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS1;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public final class FormatsKwlcMock {

    private static final FormatsKwlcMock INSTANCE = new FormatsKwlcMock();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private FormatsKwlcMock() {
    }

    public static FormatsKwlcMock getInstance() {
        return INSTANCE;
    }

    public List<FormatoKNECLCS0> getFormatoKNECLCS0s() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwlc/mock/formatoKNECLCS0.json"), new TypeReference<List<FormatoKNECLCS0>>() {
        });
    }

    public FormatoKNECLCS1 getFormatoKNECLCS1() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwlc/mock/formatoKNECLCS1.json"), FormatoKNECLCS1.class);
    }

    public FormatoKNECLCS1 getFormatoKNECLCS1Empty() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwlc/mock/formatoKNECLCS1-EMPTY.json"), FormatoKNECLCS1.class);
    }
}
