package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWLV</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwlv</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwlv</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWLV.D1200220.txt
 * KWLVVALIDACION DE LIMITES              KN        KN2CKWLV     01 KNECLVE0            KWLV  NS2000CNNNNN    SSTN    C   NNNSSNNN  NN                2020-02-20XP85643 2020-02-2012.09.25XP85643 2020-02-20-11.46.57.279813XP85643 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECLVE0.D1200220.txt
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�01�00001�CODEMP �CODIGO DE EMPRESA   �A�008�0�R�        �
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�02�00009�USUID  �IDENTIF. DE USUARIO �A�008�0�R�        �
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�03�00017�CODSERV�CODIGO DE SERVICIO  �A�004�0�R�        �
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�04�00021�CTACARG�NRO CUENTA CARGO    �A�020�0�R�        �
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�05�00041�IMPOPER�IMPORTE OPERACION   �S�017�2�R�        �
 * KNECLVE0�ENTRADA VALIDACION LIMITES    �F�06�00060�06�00058�DIVIOPE�DIVISA  OPERACION   �A�003�0�R�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECLVS0.D1200220.txt
 * KNECLVS0�SALIDA VALIDACION DE LIMITES  �X�02�00041�01�00001�INDLIMI�INDICADOR DE LIMITE �A�001�0�S�        �
 * KNECLVS0�SALIDA VALIDACION DE LIMITES  �X�02�00041�02�00002�DESCLIM�DESC. ERROR VALIDAC.�A�040�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWLV.D1200220.txt
 * KWLVKNECLVS0KNECLVS0KN2CKWLV1S                             XP85643 2020-02-20-12.04.47.741767XP85643 2020-02-20-12.04.47.741789
</pre></code>
 *
 * @see RespuestaTransaccionKwlv
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWLV",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwlv.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECLVE0.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwlv implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}
