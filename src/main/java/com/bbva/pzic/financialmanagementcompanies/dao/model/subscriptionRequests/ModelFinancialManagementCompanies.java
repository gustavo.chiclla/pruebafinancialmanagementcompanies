package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import java.util.List;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompanies {

    private ModelBusiness business;
    private ModelNetcashType netcashType;
    private ModelContract contract;
    private ModelProduct product;
    private ModelRelationType relationType;
    private List<ModelReviewerNetcash> reviewers;


    public ModelRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(ModelRelationType relationType) {
        this.relationType = relationType;
    }

    public ModelProduct getProduct() {
        return product;
    }

    public void setProduct(ModelProduct product) {
        this.product = product;
    }
    public ModelContract getContract() {
        return contract;
    }

    public void setContract(ModelContract contract) {
        this.contract = contract;
    }
    public ModelBusiness getBusiness() {
        return business;
    }

    public void setBusiness(ModelBusiness business) {
        this.business = business;
    }

    public ModelNetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(ModelNetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public List<ModelReviewerNetcash> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ModelReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}