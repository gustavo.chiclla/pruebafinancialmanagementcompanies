package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

public class ModelProductType {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
