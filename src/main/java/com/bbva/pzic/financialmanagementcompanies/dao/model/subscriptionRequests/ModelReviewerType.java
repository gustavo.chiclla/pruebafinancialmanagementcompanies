package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelReviewerType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}