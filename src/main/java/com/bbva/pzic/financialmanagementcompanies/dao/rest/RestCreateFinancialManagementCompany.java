package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 19/11/2018.
 *
 * @author Entelgy
 */
@Component
public class RestCreateFinancialManagementCompany
        extends RestPostConnection<DTOIntFinancialManagementCompanies, ModelFinancialManagementCompanyResponse> {

/*
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_URL = "servicing.url.financialManagementCompanies.createFinancialManagementCompany";
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_USE_PROXY = "servicing.proxy.financialManagementCompanies.createFinancialManagementCompany";
*/
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_URL = "servicing.smc.configuration.SMCPE1810335.backend.url";
    private static final String CREATE_FINANCIAL_MANAGEMENT_COMPANY_USE_PROXY = "servicing.smc.configuration.SMCPE1810335.backend.proxy";

    @Autowired
    private IRestCreateFinancialManagementCompanyMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(CREATE_FINANCIAL_MANAGEMENT_COMPANY_USE_PROXY, false);
    }

    public FinancialManagementCompanyId invoke(final DTOIntFinancialManagementCompanies input) {
        return mapper.mapOut(connect(CREATE_FINANCIAL_MANAGEMENT_COMPANY_URL, null, input));
    }

    @Override
    protected void evaluateResponse(ModelFinancialManagementCompanyResponse response, int statusCode) {
        evaluateMessagesResponse(response.getMessages(), "SMCPE1810335", statusCode);
    }
}
