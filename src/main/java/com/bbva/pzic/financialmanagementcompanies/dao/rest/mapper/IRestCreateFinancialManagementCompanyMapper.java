package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;

/**
 * Created on 19/11/2018.
 *
 * @author Entelgy
 */
public interface IRestCreateFinancialManagementCompanyMapper {

    ModelFinancialManagementCompanies mapIn(DTOIntFinancialManagementCompanies input);
    FinancialManagementCompanyId mapOut(ModelFinancialManagementCompanyResponse connect);
}

