package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 19/11/2018.
 *
 * @author Entelgy
 */
@Component
public class RestCreateFinancialManagementCompanyMapper
        implements IRestCreateFinancialManagementCompanyMapper {

    private static final Log LOG = LogFactory.getLog(RestCreateFinancialManagementCompanyMapper.class);

    @Override
    public ModelFinancialManagementCompanies mapIn(final DTOIntFinancialManagementCompanies input) {
        LOG.info("...called method RestCreateFinancialManagementCompanyMapper.mapIn .... ");
        ModelFinancialManagementCompanies dtoIn = new ModelFinancialManagementCompanies();
        dtoIn.setBusiness(mapInBusiness(input.getBusiness()));
        dtoIn.setNetcashType(mapInNetCashType(input.getNetcashType()));
        dtoIn.setContract(mapInContract(input.getContract()));
        dtoIn.setProduct(mapInProduct(input.getProduct()));
        dtoIn.setRelationType(mapInRelationType(input.getRelationType()));
        dtoIn.setReviewers(mapInReviewers(input.getReviewers()));
        return dtoIn;
    }

    private List<ModelReviewerNetcash> mapInReviewers(final List<DTOIntReviewerNetcash> reviewers) {
        if(CollectionUtils.isEmpty(reviewers)) return null;
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private ModelReviewerNetcash mapInReviewer(final DTOIntReviewerNetcash reviewerNetcash) {
        if(reviewerNetcash == null) return null;
        ModelReviewerNetcash dtoIn = new ModelReviewerNetcash();
        dtoIn.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
        dtoIn.setContactDetails(mapInContactDetails(reviewerNetcash.getContactDetails()));
        dtoIn.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
        dtoIn.setUnitManagement(reviewerNetcash.getUnitManagement());
        dtoIn.setBank(mapInBank(reviewerNetcash.getBank()));
        dtoIn.setProfile(mapInProfile(reviewerNetcash.getProfile()));
        dtoIn.setProfessionPosition(reviewerNetcash.getProfessionPosition());
        dtoIn.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
        return dtoIn;
    }

    private ModelProfile mapInProfile(final DTOIntProfile profile) {
        if(profile == null) return null;
        ModelProfile dtoIn = new ModelProfile();
        dtoIn.setId(profile.getId());
        return dtoIn;
    }

    private ModelBank mapInBank(final DTOIntBank bank) {
        if(bank == null) return null;
        ModelBank dtoIn = new ModelBank();
        dtoIn.setId(bank.getId());
        dtoIn.setBranch(mapInBranch(bank.getBranch()));
        return dtoIn;
    }

    private ModelBranch mapInBranch(final DTOIntBranch branch) {
        if(branch == null ) return null;
        ModelBranch dtoIn = new ModelBranch();
        dtoIn.setId(branch.getId());
        return dtoIn;
    }

    private ModelReviewerType mapInReviewerType(final DTOIntReviewerType reviewerType) {
        if(reviewerType == null) return null;
        ModelReviewerType dtoIn = new ModelReviewerType();
        dtoIn.setId(reviewerType.getId());
        return dtoIn;
    }

    private List<ModelContactDetail> mapInContactDetails(final List<DTOIntContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails)) return null;
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private ModelContactDetail mapInContactDetail(final DTOIntContactDetail contactDetail) {
        if(contactDetail == null ) return null;
        ModelContactDetail dtoIn = new ModelContactDetail();
        dtoIn.setContact(contactDetail.getContact());
        dtoIn.setContactType(contactDetail.getContactType());
        return dtoIn;
    }

    private ModelRelationType mapInRelationType(final DTOIntRelationType relationType) {
        if(relationType == null) return null;
        ModelRelationType dtoIn = new ModelRelationType();
        dtoIn.setId(relationType.getId());
        return dtoIn;
    }

    private ModelProduct mapInProduct(final DTOIntRelatedProduct product) {
        if(product == null) return null;
       ModelProduct dtoIn = new ModelProduct();
       dtoIn.setId(product.getId());
       dtoIn.setProductType(mapInProductType(product.getProductType()));
       return dtoIn;
    }

    private ModelProductType mapInProductType(final DTOIntProductType productType) {
        if(productType == null) return null;
        ModelProductType dtoIn = new ModelProductType();
        dtoIn.setId(productType.getId());
        return dtoIn;
    }


    private ModelContract mapInContract(final DTOIntContract contract) {
        if(contract == null) return null;
        ModelContract dtoIn = new ModelContract();
        dtoIn.setId(contract.getId());
        return dtoIn;
    }

    private ModelNetcashType mapInNetCashType(final DTOIntNetcashType netcashType) {
        if(netcashType == null) return null;
        ModelNetcashType dtoIn = new ModelNetcashType();
        dtoIn.setId(netcashType.getId());
        dtoIn.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoIn;
    }

    private ModelVersion mapInVersion(final DTOIntVersionProduct version) {
        if(version == null) return null;
        ModelVersion dtoIn = new ModelVersion();
        dtoIn.setId(version.getId());
        return dtoIn;
    }

    private ModelBusiness mapInBusiness(final DTOIntBusiness business) {
        if (business  == null) return null;
        ModelBusiness dtoIn = new ModelBusiness();
        dtoIn.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoIn.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoIn.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        return dtoIn;
    }

    private ModelLimitAmount mapInLimitAmount(final DTOIntLimitAmount limitAmount) {
        if(limitAmount == null) return null;
        ModelLimitAmount dtoIn = new ModelLimitAmount();
        dtoIn.setAmount(limitAmount.getAmount());
        dtoIn.setCurrency(limitAmount.getCurrency());
        return dtoIn;
    }

    private ModelBusinessManagement mapInBusinessManagement(final DTOIntBusinessManagement businessManagement) {
        if(businessManagement == null) return null;
        ModelBusinessManagement dtoIn = new ModelBusinessManagement();
        dtoIn.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoIn;
    }

    private ModelManagementType mapInManagementType(final DTOIntManagementType managementType) {
        if(managementType == null) return null;
        ModelManagementType dtoIn = new ModelManagementType();
        dtoIn.setId(managementType.getId());
        return dtoIn;
    }

    private List<ModelBusinessDocument> mapInBusinessDocuments(final List<DTOIntBusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)) return null;
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private ModelBusinessDocument mapInBusinessDocument(final DTOIntBusinessDocument businessDocument) {
        if(businessDocument == null) return null;
        ModelBusinessDocument dtoIn = new ModelBusinessDocument();
        dtoIn.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
        dtoIn.setDocumentNumber(businessDocument.getDocumentNumber());
        dtoIn.setIssueDate(businessDocument.getIssueDate());
        dtoIn.setExpirationDate(businessDocument.getExpirationDate());
        return dtoIn;
    }

    private ModelBusinessDocumentType mapInBusinessDocumentType(final DTOIntBusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null) return null;
        ModelBusinessDocumentType dtoIn = new ModelBusinessDocumentType();
        dtoIn.setId(businessDocumentType.getId());
        return dtoIn;
    }

    @Override
    public FinancialManagementCompanyId mapOut(ModelFinancialManagementCompanyResponse connect) {
        LOG.info("... called method RestCreateFinancialManagementCompanyMapper.mapOut ...");
        if (connect == null || connect.getData() == null) {
            return null;
        }
        FinancialManagementCompanyId response = new FinancialManagementCompanyId();
        response.setId(connect.getData().getId());
        return response;
    }
}
