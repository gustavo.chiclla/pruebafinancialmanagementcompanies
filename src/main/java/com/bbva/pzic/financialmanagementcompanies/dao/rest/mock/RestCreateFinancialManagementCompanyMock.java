package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestCreateFinancialManagementCompany;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 19/11/2018.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestCreateFinancialManagementCompanyMock extends RestCreateFinancialManagementCompany {

    public static final String EMPTY_RESPONSE = "1111111";

    public static final String ERROR_RESPONSE = "9999999";

    @Override
    protected ModelFinancialManagementCompanyResponse connect(String urlPropertyValue, Map<String, String> pathParams, DTOIntFinancialManagementCompanies entityPayload) {

        if (EMPTY_RESPONSE.equals(entityPayload.getProduct().getId())) {
            return new ModelFinancialManagementCompanyResponse();
        }

        ModelFinancialManagementCompanyResponse response = ResponseFinancialManagementCompaniesMock.getInstance()
                .buildModelFinancialManagementCompanyResponse();

        int statusCode = 200;
        List<Message> messages = new ArrayList<>();

        if (ERROR_RESPONSE.equals(entityPayload.getProduct().getId())) {
            messages.add(ResponseFinancialManagementCompaniesMock.getInstance().getErrorMessage());
            statusCode = 500;
        }

        response.setMessages(messages);

        evaluateResponse(response, statusCode);

        return response;
    }

}
