package com.bbva.pzic.financialmanagementcompanies.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequestData;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.RestGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

import static com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl.RestGetSubscriptionRequestMapper.PATH_PARAM_SUBSCRIPTION_REQUEST_ID;

/**
 * Created on 23/03/2018.
 *
 * @author Entelgy
 */
@Component
@Primary
public class RestGetSubscriptionRequestMock extends RestGetSubscriptionRequest {

    public static final String EMPTY_RESPONSE = "666";
    public static final String EXPAND_ONLY_REVIEWERS_RESPONSE = "777";
    public static final String EXPAND_ONLY_BUSINESS_MANAGERS_RESPONSE = "888";
    public static final String WITHOUT_EXPANDS_RESPONSE = "999";

    @Override
    public ModelSubscriptionRequestData connect(final String urlPropertyValue, final Map<String, String> params) {
        String subscriptionRequestId = params.get(PATH_PARAM_SUBSCRIPTION_REQUEST_ID);
        try {
            if (EMPTY_RESPONSE.equals(subscriptionRequestId)) {
                return null;
            }

            ModelSubscriptionRequestData modelSubscriptionRequestData = ResponseFinancialManagementCompaniesMock.getInstance().buildResponseModelSubscriptionRequestData();

            if (EXPAND_ONLY_REVIEWERS_RESPONSE.equals(subscriptionRequestId)) {
                modelSubscriptionRequestData.getData().setBusinessManagers(null);

            } else if (EXPAND_ONLY_BUSINESS_MANAGERS_RESPONSE.equals(subscriptionRequestId)) {
                modelSubscriptionRequestData.getData().setParticipants(null);

            } else if (WITHOUT_EXPANDS_RESPONSE.equals(subscriptionRequestId)) {
                modelSubscriptionRequestData.getData().setParticipants(null);
                modelSubscriptionRequestData.getData().setBusinessManagers(null);
            }

            return modelSubscriptionRequestData;

        } catch (IOException e) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
        }
    }
}