package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxGetFMCAuthorizedBusinessManagerProfiledServiceMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.converter.builtin.BooleanToStringConverter;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxGetFMCAuthorizedBusinessManagerProfiledServiceMapper
        extends ConfigurableMapper
        implements ITxGetFMCAuthorizedBusinessManagerProfiledServiceMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.getConverterFactory().registerConverter(new BooleanToStringConverter());

        factory.classMap(InputGetFMCAuthorizedBusinessManagerProfiledService.class, FormatoKNECFPE0.class)
                .field("financialManagementCompanyId", "compaid")
                .field("authorizedBusinessManagerId", "managid")
                .field("profiledServiceId", "codser")
                .register();

        factory.classMap(FormatoKNECFPS0.class, ProfiledService.class)
                .field("idecon", "id")
                .field("servic", "service.id")
                .field("servdes", "service.name")
                .field("indser", "operationRights.indicators.id")
                .field("desind", "operationRights.indicators.name")
                .field("permide", "operationRights.permissionType.id")
                .field("permdes", "operationRights.permissionType.name")
                .field("powerid", "operationRights.signature.validationRights.id")
                .field("powedes", "operationRights.signature.validationRights.name")
                .field("amount", "operationRights.signature.amountLimit.amount")
                .field("curency", "operationRights.signature.amountLimit.currency")
                .field("empaud", "hasAssignedAuditor")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECFPE0 mapIn(
            final InputGetFMCAuthorizedBusinessManagerProfiledService dtoIn) {
        return map(dtoIn, FormatoKNECFPE0.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProfiledService mapOut(final FormatoKNECFPS0 formatOutput) {
        ProfiledService profiledService = map(formatOutput, ProfiledService.class);

        if (formatOutput.getIndser() != null) {
            profiledService.getOperationRights().getIndicators().setId(
                    translator.translateBackendEnumValueStrictly(
                            "authorizedBusinessManager.operationsRights.indicators.id", formatOutput.getIndser()));
        }

        if (formatOutput.getPermide() != null) {
            profiledService.getOperationRights().getPermissionType().setId(
                    translator.translateBackendEnumValueStrictly(
                            "authorizedBusinessManager.operationsRights.permissionType.id", formatOutput.getPermide()));
        }

        if (formatOutput.getPowerid() != null) {
            profiledService.getOperationRights().getSignature().getValidationRights().setId(
                    translator.translateBackendEnumValueStrictly(
                            "authorizedBusinessManager.signature.id", formatOutput.getPowerid()));
        }

        return profiledService;
    }
}
