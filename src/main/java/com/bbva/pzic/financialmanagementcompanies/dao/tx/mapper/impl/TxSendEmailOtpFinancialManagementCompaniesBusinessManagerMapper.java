package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBEHF;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf.FormatoKNECBSHF;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper
public class TxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper extends ConfigurableMapper implements ITxSendEmailOtpFinancialManagementCompaniesBusinessManagerMapper {

    private static final String ENUM_BUSINESS_MANAGER_CONTACT_TYPE_ID = "businessManagers.contactType.id";

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputSendEmailOtpFinancialManagementCompaniesBusinessManager.class, FormatoKNECBEHF.class)
                .field("businessManagerId", "usuid")
                .register();

        factory.classMap(FormatoKNECBSHF.class, InputSendEmailOtpFinancialManagementCompaniesBusinessManager.class)
                .field("nomemp", "businessName")
                .field("nomusu", "userName")
                .field("tipco0", "emailList[0].type.id")
                .field("daco0", "emailList[0].value")
                .field("tipco1", "emailList[1].type.id")
                .field("daco1", "emailList[1].value")
                .field("tipco2", "emailList[2].type.id")
                .field("daco2", "emailList[2].value")
                .field("tipco3", "emailList[3].type.id")
                .field("daco3", "emailList[3].value")
                .register();
    }

    @Override
    public FormatoKNECBEHF mapIn(InputSendEmailOtpFinancialManagementCompaniesBusinessManager input) {
        return map(input, FormatoKNECBEHF.class);
    }

    @Override
    public InputSendEmailOtpFinancialManagementCompaniesBusinessManager mapOut(FormatoKNECBSHF formatOutput) {
        InputSendEmailOtpFinancialManagementCompaniesBusinessManager result = map(formatOutput, InputSendEmailOtpFinancialManagementCompaniesBusinessManager.class);

        if (result.getEmailList() != null && !result.getEmailList().isEmpty()) {
            for (int i = 0; i < result.getEmailList().size(); i++) {
                switch (i) {
                    case 0:
                        result.getEmailList().get(i).getType().setId(enumMapper.getEnumValue(ENUM_BUSINESS_MANAGER_CONTACT_TYPE_ID, formatOutput.getTipco0()));
                        break;
                    case 1:
                        result.getEmailList().get(i).getType().setId(enumMapper.getEnumValue(ENUM_BUSINESS_MANAGER_CONTACT_TYPE_ID, formatOutput.getTipco1()));
                        break;
                    case 2:
                        result.getEmailList().get(i).getType().setId(enumMapper.getEnumValue(ENUM_BUSINESS_MANAGER_CONTACT_TYPE_ID, formatOutput.getTipco2()));
                        break;
                    case 3:
                        result.getEmailList().get(i).getType().setId(enumMapper.getEnumValue(ENUM_BUSINESS_MANAGER_CONTACT_TYPE_ID, formatOutput.getTipco3()));
                        break;
                    default:
                        break;
                }
            }
        }

        return result;
    }
}
