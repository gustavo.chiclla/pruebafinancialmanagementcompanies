package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@Mapper
public class TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper
        extends ConfigurableMapper
        implements ITxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility.class, FormatoKNECLVE0.class)
                .field("financialManagementCompanyId", "codemp")
                .field("authorizedBusinessManagerId", "usuid")
                .field("profiledServiceId", "codserv")
                .field("validateOperationFeasibility.contract.id", "ctacarg")
                .field("validateOperationFeasibility.operationAmount.amount", "impoper")
                .field("validateOperationFeasibility.operationAmount.currency", "diviope")
                .register();

        factory.classMap(FormatoKNECLVS0.class, ValidateOperationFeasibility.class)
                .field("indlimi", "result.id")
                .field("desclim", "result.reason")
                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECLVE0 mapIn(
            final InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility dtoIn) {
        return map(dtoIn, FormatoKNECLVE0.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidateOperationFeasibility mapOut(
            final FormatoKNECLVS0 formatOutput) {
        ValidateOperationFeasibility validateOperationFeasibility = map(formatOutput, ValidateOperationFeasibility.class);

        if (formatOutput.getIndlimi() != null) {
            validateOperationFeasibility.getResult().setId(
                    translator.translateBackendEnumValueStrictly("profiledServices.validateOperationFeasibility.result.id", formatOutput.getIndlimi()));
        }

        return validateOperationFeasibility;
    }
}
