package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "createAuthorizedBusinessManager", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "createAuthorizedBusinessManager", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreateAuthorizedBusinessManager implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Information about authorized business manager.
     */
    private BusinessManager businessManager;
    /**
     * Information about managed business.
     */
    private BusinessManagement businessManagement;
    /**
     * Authorized business manager unique identifier.
     */
    private String id;

    private String status;

    public BusinessManager getBusinessManager() {
        return businessManager;
    }

    public void setBusinessManager(BusinessManager businessManager) {
        this.businessManager = businessManager;
    }

    public BusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(BusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
