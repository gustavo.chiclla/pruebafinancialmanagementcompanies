package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "statusSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "statusSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusSubscriptionRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Status identifier.
     */
    private String id;
    /**
     * Status name.
     */
    private String name;
    /**
     * Reason for the transition of solicitude states.
     */
    private String reason;
    /**
     * Update date. Date format ISO-8601 (YYYY-MM-DD).
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
