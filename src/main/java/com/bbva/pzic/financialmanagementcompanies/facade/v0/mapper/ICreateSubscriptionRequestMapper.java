package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequestId;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface ICreateSubscriptionRequestMapper {

    SubscriptionRequest mapIn(SubscriptionRequest subscriptionRequest);

    ServiceResponse<SubscriptionRequestId> mapOut(String identifier);
}
