package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface IGetFMCAuthorizedBusinessManagerProfiledServiceMapper {

    InputGetFMCAuthorizedBusinessManagerProfiledService mapIn(
            String financialManagementCompanyId,
            String authorizedBusinessManagerId, String profiledServiceId);

    ServiceResponse<ProfiledService> mapOut(ProfiledService profiledService);
}
