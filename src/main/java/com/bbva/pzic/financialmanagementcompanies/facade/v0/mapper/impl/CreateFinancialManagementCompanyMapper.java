package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompanyMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.bbva.pzic.routine.translator.facade.Translator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CreateFinancialManagementCompanyMapper implements ICreateFinancialManagementCompanyMapper {
    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompanyMapper.class);

//    @Autowired
    private final Translator translator;

    public CreateFinancialManagementCompanyMapper (Translator translator){
        this.translator = translator;
    }

    @Override
    public DTOIntFinancialManagementCompanies mapIn(final FinancialManagementCompanies financialManagementCompanies) {
        LOG.info("..... called method CreateFinancialManagementCompanyMapper.mapIn .....");
        DTOIntFinancialManagementCompanies dtoIn = new DTOIntFinancialManagementCompanies();
        dtoIn.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
        dtoIn.setNetcashType(mapInNetCashType(financialManagementCompanies.getNetcashType()));
        dtoIn.setContract(mapInContract(financialManagementCompanies.getContract()));
        dtoIn.setProduct(mapInProduct(financialManagementCompanies.getProduct()));
        dtoIn.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
        dtoIn.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));
        return dtoIn;
    }

    private List<DTOIntReviewerNetcash> mapInReviewers(final List<ReviewerNetcash> reviewers) {
        if(CollectionUtils.isEmpty(reviewers)) return null;
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }
    private DTOIntReviewerNetcash mapInReviewer(final ReviewerNetcash reviewers){
       if(reviewers ==null)  return null;
       DTOIntReviewerNetcash dtoIn = new DTOIntReviewerNetcash();
       dtoIn.setBusinessAgentId(reviewers.getBusinessAgentId());
       dtoIn.setContactDetails(mapInContactDetails(reviewers.getContactDetails()));
       dtoIn.setReviewerType(mapInReviewerType(reviewers.getReviewerType()));
       dtoIn.setUnitManagement(reviewers.getUnitManagement());
       dtoIn.setBank(mapInBank(reviewers.getBank()));
       dtoIn.setProfile(mapInProfile(reviewers.getProfile()));
       dtoIn.setProfessionPosition(reviewers.getProfessionPosition());
       dtoIn.setRegistrationIdentifier(reviewers.getRegistrationIdentifier());
       return dtoIn;
    }

    private DTOIntProfile mapInProfile(final Profile profile) {
        if(profile == null) return null;
        DTOIntProfile dtoIn = new DTOIntProfile();
        dtoIn.setId(profile.getId());
        return dtoIn;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if(bank == null) return null;
        DTOIntBank dtoIn= new DTOIntBank();
        dtoIn.setId(bank.getId());
        dtoIn.setBranch(mapInBranch(bank.getBranch()));
        return dtoIn;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if(branch == null) return null;
        DTOIntBranch dtoIn= new DTOIntBranch();
        dtoIn.setId(branch.getId());
        return dtoIn;
    }

    private DTOIntReviewerType mapInReviewerType(final ReviewerType reviewerType) {
        if(reviewerType == null) return null;
        DTOIntReviewerType dtoIn = new DTOIntReviewerType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", reviewerType.getId()));
        return dtoIn;
    }

    private List<DTOIntContactDetail> mapInContactDetails(final List<ContactDetail> contactDetails) {
        if(contactDetails == null) return null;
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private DTOIntContactDetail mapInContactDetail(final ContactDetail contactDetail) {
        if(contactDetail == null) return null;
        DTOIntContactDetail dtoIn = new DTOIntContactDetail();
        dtoIn.setContact(contactDetail.getContact());
        dtoIn.setContactType(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id",contactDetail.getContactType()));
        return dtoIn;
    }

    private DTOIntRelationType mapInRelationType(final RelationType relationType) {
        if (relationType == null) return null;
        DTOIntRelationType dtoIn = new DTOIntRelationType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType",relationType.getId()));
        return dtoIn;
    }

    private DTOIntRelatedProduct mapInProduct(final RelatedProduct product) {
        if (product == null) return null;
        DTOIntRelatedProduct dtoIn = new DTOIntRelatedProduct();
        dtoIn.setId(product.getId());
        dtoIn.setProductType(mapInProductType(product.getProductType()));
        return dtoIn;
    }

    private DTOIntProductType mapInProductType(final ProductType productType) {
        if (productType == null) return null;
        DTOIntProductType dtoIn = new DTOIntProductType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", productType.getId()));
        return dtoIn;
    }

    private DTOIntContract mapInContract(final Contract contract) {
        if (contract == null) return null;
        DTOIntContract dtoIn = new DTOIntContract();
        dtoIn.setId(contract.getId());
        return dtoIn;
    }

    private DTOIntNetcashType mapInNetCashType(final NetcashType netcashType) {
        if(netcashType == null) return null;

        DTOIntNetcashType dtoIn = new DTOIntNetcashType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id",netcashType.getId()));
        dtoIn.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoIn;
    }

    private DTOIntVersionProduct mapInVersion(final Version version) {
        if(version == null) return null;
        DTOIntVersionProduct dtoIn = new DTOIntVersionProduct();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", version.getId()));
        return dtoIn;
    }

    private DTOIntBusiness mapInBusiness(final Business business) {
        if(business == null) return null;

        DTOIntBusiness dtoIn = new DTOIntBusiness();
        dtoIn.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoIn.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoIn.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        return dtoIn;
    }

    private DTOIntLimitAmount mapInLimitAmount(final LimitAmount limitAmount) {
        if(limitAmount == null) return null;
        DTOIntLimitAmount dtoIn = new DTOIntLimitAmount();
        dtoIn.setAmount(limitAmount.getAmount());
        dtoIn.setCurrency(limitAmount.getCurrency());
        return dtoIn;
    }

    private DTOIntBusinessManagement mapInBusinessManagement(final BusinessManagement businessManagement) {
        if(businessManagement == null ) return null;
        DTOIntBusinessManagement dtoIn= new DTOIntBusinessManagement();
        dtoIn.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoIn;
    }

    private DTOIntManagementType mapInManagementType(final ManagementType managementType) {
        if(managementType == null) return null;

        DTOIntManagementType dtoIn= new DTOIntManagementType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",managementType.getId()));
//        dtoIn.setId(managementType.getId());
        return dtoIn;

    }

    private List<DTOIntBusinessDocument> mapInBusinessDocuments(final List<BusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }
    private DTOIntBusinessDocument mapInBusinessDocument (final BusinessDocument businessDocument ){
       if(businessDocument == null) return null;

       DTOIntBusinessDocument dtoIn = new DTOIntBusinessDocument();
       dtoIn.setBusinessDocumentType(mapInBusinessDocumentType(businessDocument.getBusinessDocumentType()));
       dtoIn.setDocumentNumber(businessDocument.getDocumentNumber());
       dtoIn.setIssueDate(businessDocument.getIssueDate());
       dtoIn.setExpirationDate(businessDocument.getExpirationDate());
       return dtoIn;
    }

    private DTOIntBusinessDocumentType mapInBusinessDocumentType(final BusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null) {
            return null;
        }

        DTOIntBusinessDocumentType dtoIn = new DTOIntBusinessDocumentType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly("documentType.id",businessDocumentType.getId()));
//        dtoIn.setId(businessDocumentType.getId());
        return dtoIn;
    }

    @Override
    public ServiceResponse<FinancialManagementCompanyId> mapOut(final FinancialManagementCompanyId input) {
        LOG.info("... called method CreateFinancialManagementCompanyMapper.mapOut ...");

        if(input == null){
            return null;
        }
        FinancialManagementCompanyId requestId = new FinancialManagementCompanyId();
        requestId.setId(input.getId());

//        return ServiceResponse.data(requestId).build();
        return ServiceResponse.data(requestId).build();
    }
}
