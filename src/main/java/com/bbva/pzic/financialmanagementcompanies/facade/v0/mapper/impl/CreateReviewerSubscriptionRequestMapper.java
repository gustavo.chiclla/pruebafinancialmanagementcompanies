package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateReviewerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ContactDetail;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.Reviewer;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ReviewerId;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateReviewerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class CreateReviewerSubscriptionRequestMapper extends ConfigurableMapper
        implements
        ICreateReviewerSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(CreateReviewerSubscriptionRequestMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputCreateReviewerSubscriptionRequest mapIn(final String subscriptionRequestId, final Reviewer reviewer) {
        LOG.info("... called method CreateReviewerSubscriptionRequestMapper.mapIn ...");
        InputCreateReviewerSubscriptionRequest input = new InputCreateReviewerSubscriptionRequest();

        input.setSubscriptionRequestId(subscriptionRequestId);
        input.setReviewer(reviewer);

        if (reviewer.getReviewerType() != null) {
            String reviewerTypeId = enumMapper.getBackendValue("suscriptionRequest.reviewer.id", reviewer.getReviewerType().getId());
            input.getReviewer().getReviewerType().setId(reviewerTypeId);
        }
        input.getReviewer().setContactDetails(enumMapContactDetails(reviewer.getContactDetails()));

        return input;
    }

    private List<ContactDetail> enumMapContactDetails(List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return contactDetails;
        }
        for (ContactDetail canonic : contactDetails) {
            canonic.setContactType(enumMapper.getBackendValue("contactDetails.contactType.id", canonic.getContactType()));
        }
        return contactDetails;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<ReviewerId> mapOut(final String identifier) {
        LOG.info("... called method CreateReviewerSubscriptionRequestMapper.mapOut ...");
        ReviewerId response = new ReviewerId();
        response.setId(identifier);
        return ServiceResponse.data(response).build();
    }
}
