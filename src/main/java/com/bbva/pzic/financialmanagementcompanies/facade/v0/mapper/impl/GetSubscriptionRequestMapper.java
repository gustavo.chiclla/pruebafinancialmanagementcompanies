package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IGetSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class GetSubscriptionRequestMapper implements IGetSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(GetSubscriptionRequestMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputGetSubscriptionRequest mapIn(final String subscriptionRequestId) {
        LOG.info("... called method GetSubscriptionRequestMapper.mapIn ...");
        InputGetSubscriptionRequest input = new InputGetSubscriptionRequest();
        input.setSubscriptionRequestId(subscriptionRequestId);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public ServiceResponse<SubscriptionRequest> mapOut(final SubscriptionRequest data) {
        LOG.info("... called method GetSubscriptionRequestMapper.mapOut ...");
        return ServiceResponse.data(data).pagination(null).build();
    }
}
