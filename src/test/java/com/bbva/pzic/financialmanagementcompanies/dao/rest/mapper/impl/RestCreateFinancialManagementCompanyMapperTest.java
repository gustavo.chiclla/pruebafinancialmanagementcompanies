package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mock.stubs.ResponseFinancialManagementCompaniesMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created on 19/11/2018.
 *
 * @author Entelgy
 */
public class RestCreateFinancialManagementCompanyMapperTest {

    private IRestCreateFinancialManagementCompanyMapper mapper;

    @Before
    public void setUp() {
        mapper = new RestCreateFinancialManagementCompanyMapper();
    }

    @Test
    public void mapOutFullTest() {
        ModelFinancialManagementCompanyResponse input = ResponseFinancialManagementCompaniesMock.getInstance()
                .buildModelFinancialManagementCompanyResponse();
        FinancialManagementCompanyId result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    public void mapOutEmptyTest() {
        FinancialManagementCompanyId result = mapper.mapOut(new ModelFinancialManagementCompanyResponse());
        assertNull(result);

        result = mapper.mapOut(null);
        assertNull(result);
    }

}
