package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanyId;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateFinancialManagementCompanyMapperTest {

    private static final String FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND = "B";
    private static final String FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND = "R";
    private static final String SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND = "S";

    @InjectMocks
    private CreateFinancialManagementCompanyMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateFrontendEnumValueStrictly("documentType.id", "RUC")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",
                "JOINT")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", "NETCASH_BUSINESS")).thenReturn(PRODUCT_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", "ADVANCED")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", "CARDS")).thenReturn(PRODUCT_TYPE_BACKEND_C);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", "LINKED_WITH")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", "DEPUTY_MANAGER")).thenReturn(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "EMAIL")).thenReturn("MA");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "PHONE_NUMBER")).thenReturn("TF");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "MOBILE_NUMBER")).thenReturn("MV");

    }

    @Test
    public void mapInFullTest() throws IOException {

        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies();

        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);

        assertNotNull(result);

        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContactType());

        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals(PRODUCT_ID_BACKEND, result.getNetcashType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND, result.getNetcashType().getVersion().getId());
        assertEquals(input.getContract().getId(), result.getContract().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(PRODUCT_TYPE_BACKEND_C, result.getProduct().getProductType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND, result.getRelationType().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(0).getContactType(), "MA");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(1).getContact(), result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(1).getContactType(), "TF");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(2).getContact(), result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(2).getContactType(), "MV");

    }

    @Test
    public void mapInFullWithoutBusinessDocumentsTest() throws IOException {
        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies();
        input.getBusiness().setBusinessDocuments(null);

//        when(translator.translateFrontendEnumValueStrictly("documentType.id", "RUC")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",
                "JOINT")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", "NETCASH_BUSINESS")).thenReturn(PRODUCT_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", "ADVANCED")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND);
//        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", "CARDS")).thenReturn(PRODUCT_TYPE_BACKEND_C);
//        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", "LINKED_WITH")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", "DEPUTY_MANAGER")).thenReturn(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "EMAIL")).thenReturn("MA");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "PHONE_NUMBER")).thenReturn("TF");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "MOBILE_NUMBER")).thenReturn("MV");


        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);

        assertNotNull(result);
        assertNull(result.getBusiness().getBusinessDocuments());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContactType());

        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals(PRODUCT_ID_BACKEND, result.getNetcashType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND, result.getNetcashType().getVersion().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(0).getContactType(), "MA");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(1).getContact(), result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(1).getContactType(), "TF");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(2).getContact(), result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(2).getContactType(), "MV");

    }

    @Test
    public void mapInFullWithoutBusinessManagementTest() throws IOException {
        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies();
        input.getBusiness().setBusinessManagement(null);

        when(translator.translateFrontendEnumValueStrictly("documentType.id", "RUC")).thenReturn(RUC_DOCUMENT_TYPE_ID_BACKEND);
/*
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",
                "JOINT")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND);
*/
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", "NETCASH_BUSINESS")).thenReturn(PRODUCT_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", "ADVANCED")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND);
/*
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", "CARDS")).thenReturn(PRODUCT_TYPE_BACKEND_C);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", "LINKED_WITH")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
*/
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", "DEPUTY_MANAGER")).thenReturn(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "EMAIL")).thenReturn("MA");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "PHONE_NUMBER")).thenReturn("TF");
        when(translator.translateFrontendEnumValueStrictly("contactDetails.contactType.id", "MOBILE_NUMBER")).thenReturn("MV");

        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNull(result.getBusiness().getBusinessManagement());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());

        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(1).getContactType());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(2).getContactType());


        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals(PRODUCT_ID_BACKEND, result.getNetcashType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND, result.getNetcashType().getVersion().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(0).getContactType(), "MA");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(1).getContact(), result.getReviewers().get(0).getContactDetails().get(1).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(1).getContactType(), "TF");
        assertEquals(input.getReviewers().get(0).getContactDetails().get(2).getContact(), result.getReviewers().get(0).getContactDetails().get(2).getContact());
        assertEquals(result.getReviewers().get(0).getContactDetails().get(2).getContactType(), "MV");

    }

    @Test
    public void mapInFullWithoutContactDetailsTest() throws IOException {
        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies();

        when(translator.translateFrontendEnumValueStrictly("documentType.id", "RUC")).thenReturn(RUC_DOCUMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.businessManagement.managementType.id",
                "JOINT")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_BUSINESS_MANAGEMENT_TYPE_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.product.id", "NETCASH_BUSINESS")).thenReturn(PRODUCT_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.version.id", "ADVANCED")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.productType.id", "CARDS")).thenReturn(PRODUCT_TYPE_BACKEND_C);
        when(translator.translateFrontendEnumValueStrictly("financialManagementCompany.relatedContracts.relationType", "LINKED_WITH")).thenReturn(FINANCIAL_MANAGEMENT_COMPANY_RELATED_CONTRACTS_RELATION_TYPE_BACKEND);
        when(translator.translateFrontendEnumValueStrictly("suscriptionRequest.reviewer.id", "DEPUTY_MANAGER")).thenReturn(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND);

        input.getReviewers().get(0).setContactDetails(null);
        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessManagement());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());
        assertNull(result.getReviewers().get(0).getContactDetails());


        assertEquals(RUC_DOCUMENT_TYPE_ID_BACKEND, result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals(PRODUCT_ID_BACKEND, result.getNetcashType().getId());
        assertEquals(FINANCIAL_MANAGEMENT_COMPANY_VERSION_ID_BACKEND, result.getNetcashType().getVersion().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(SUSCRIPTION_REQUEST_REVIEWER_ID_BACKEND, result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntFinancialManagementCompanies result = mapper.mapIn(new FinancialManagementCompanies());
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getNetcashType());
        assertNull(result.getReviewers());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<FinancialManagementCompanyId> result = mapper.mapOut(new FinancialManagementCompanyId());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<FinancialManagementCompanyId> result = mapper.mapOut(null);
        assertNull(result);
    }

}
