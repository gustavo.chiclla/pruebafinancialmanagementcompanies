package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static com.bbva.pzic.financialmanagementcompanies.EntityStubs.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class ModifySubscriptionRequestMapperTest {

    @InjectMocks
    private ModifySubscriptionRequestMapper mapper;
    @Mock
    private EnumMapper enumMapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mapBackendValues();
    }

    private void mapBackendValues() {
        Mockito.when(enumMapper.getBackendValue("suscriptionRequest.status.id", APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_ENUM)).thenReturn(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND);
        Mockito.when(enumMapper.getBackendValue("suscriptionRequest.status.reason.id", INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_ENUM)).thenReturn(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_BACKEND);
    }

    @Test
    public void mapInFullTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        InputModifySubscriptionRequest result = mapper.mapIn(
                EntityStubs.SUBSCRIPTION_REQUEST_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSubscriptionRequestId());
        Assert.assertNotNull(result.getSubscriptionRequest().getStatus()
                .getId());
        Assert.assertNotNull(result.getSubscriptionRequest().getStatus()
                .getReason());
        Assert.assertNotNull(result.getSubscriptionRequest().getComment());
        Assert.assertEquals(EntityStubs.SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());
        Assert.assertEquals(APPROVED_SUSCRIPTION_REQUEST_STATUS_ID_BACKEND, result
                .getSubscriptionRequest().getStatus().getId());
        Assert.assertEquals(INVALID_DOCUMENT_NUMBER_SUSCRIPTION_REQUEST_STATUS_REASON_ID_BACKEND, result
                .getSubscriptionRequest().getStatus().getReason());
        Assert.assertEquals(input.getComment(), result.getSubscriptionRequest()
                .getComment());
    }

    @Test
    public void mapInWithoutStatusTest() throws IOException {
        SubscriptionRequest input = EntityStubs.getInstance()
                .getSubscriptionRequest();
        input.setStatus(null);

        InputModifySubscriptionRequest result = mapper.mapIn(
                EntityStubs.SUBSCRIPTION_REQUEST_ID, input);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSubscriptionRequestId());
        Assert.assertNull(result.getSubscriptionRequest().getStatus());
        Assert.assertNotNull(result.getSubscriptionRequest().getComment());
        Assert.assertEquals(EntityStubs.SUBSCRIPTION_REQUEST_ID,
                result.getSubscriptionRequestId());

        Assert.assertEquals(input.getComment(), result.getSubscriptionRequest()
                .getComment());
    }

    @Test
    public void mapInEmptyTest() {
        InputModifySubscriptionRequest result = mapper.mapIn(
                EntityStubs.SUBSCRIPTION_REQUEST_ID, new SubscriptionRequest());
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getSubscriptionRequest());
        Assert.assertNotNull(result.getSubscriptionRequestId());
        Assert.assertNull(result.getSubscriptionRequest().getStatus());
        Assert.assertNull(result.getSubscriptionRequest().getComment());
    }
}
